import React, { useEffect } from 'react';
import { AiFillEdit } from 'react-icons/ai';
import { AiFillDelete } from 'react-icons/ai';
import { AiOutlineCheckCircle } from 'react-icons/ai';

import "./App.css";

function Todo({ todo, index, completeTodo, removeTodo, updateTodo }) {
    return ( <
        div className = "todo"
        style = {
            { textDecoration: todo.isCompleted ? "line-through" : "" } } >
        { todo.text } <
        div >
        <
        button className = "btn-complete"
        onClick = {
            () => completeTodo(index) } > < AiOutlineCheckCircle / > < /button> <
        button className = "btn-update"
        onClick = {
            () => updateTodo(index) } > < AiFillEdit / > < /button> <
        button className = "btn-remove"
        onClick = {
            () => removeTodo(index) } > < AiFillDelete / > < /button>

        <
        /div> <
        /div>
    );
}

function TodoForm({ addTodo }) {
    const [value, setValue] = React.useState("");

    const handleSubmit = e => {
        e.preventDefault();
        if (!value) return;
        addTodo(value);
        setValue("");
    };

    return ( <
        form onSubmit = { handleSubmit } >
        <
        input type = "text"
        className = "input"
        value = { value }
        onChange = { e => setValue(e.target.value) }
        /> <
        /form>
    );
}